import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import breeze.linalg.DenseVector

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.mllib.classification._
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.rdd.RDD
import onevrest._

object SVM_Main {
  def main(args: Array[String]) {

val conf = new SparkConf().setAppName("SVM_Multiclass")
val sc = new SparkContext(conf)

// Load training data in LIBSVM format.
val data = MLUtils.loadLibSVMFile(sc, "hdfs://master:9000/SVM_data/sample_multiclass_classification_data.txt")


// Split data into training (60%) and test (40%).
//val splits = data.randomSplit(Array(0.6, 0.4), seed = 11L)
//val trainingData = splits(0).cache()
//val testData = splits(1)

val splits = data.randomSplit(Array(0.7, 0.3))
val (trainingData, testData) = (splits(0), splits(1))


//number of iterations
val numIterations = 100

//printing TrainingData and test Data
println("TrainingData" + trainingData.toArray().foreach(line => println(line)))
println("testData" + testData.toArray().foreach(line => println(line)))


//Get number of Classes
val numClasses = trainingData.map(_.label).max().toInt

val classModels = (0 until numClasses).map { classId =>
val trainingInput = trainingData.map { case LabeledPoint(label, features) =>
LabeledPoint(if (label == (classId+1)) 1.0 else 0.0, features)}.cache() 		

//Printing interm trainingInput 
println("trainingInput" + trainingInput.toArray().foreach(line => println(line)))
println("Class Id" + classId)

val model = SVMWithSGD.train(trainingInput, numIterations, 1.0, 0.01, 1.0)
trainingInput.unpersist(false)
model.clearThreshold()
}.toArray
val model = new SVMMultiClassOVAModel(classModels)
/**/


val scoreAndLabels = testData.map { point =>
val score = model.predict(point.features)
(score, point.label)
} 

model.output();

println("Score Labels" + scoreAndLabels.toArray().foreach(line => println(line)))
/**/

val metrics = new MulticlassMetrics(scoreAndLabels)
println("Confusion Matrix")
println(metrics.confusionMatrix)
//println("Labels" + metrics.labels.deep.mkString("\n"))
}
}



