import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import org.jsoup.Jsoup;  
import org.jsoup.nodes.Document; 
import org.jsoup.select.Elements;

public class Main {
  public static void main(String[] args) throws IOException {
    //File file = new File(args[0]);
    File file = new File("LinksFile.txt");
    if (!file.exists()) {
      System.out.println(args[0] + " does not exist.");
      return;
    }
    if (!(file.isFile() && file.canRead())) {
      System.out.println(file.getName() + " cannot be read from.");
      return;
    }
    
    FileReader fileReader = new FileReader(file);
    FileInputStream fis = new FileInputStream(file);
    
    char current;
    int i,pageRank=0;
    Scanner scnr = new Scanner(file , "UTF-8");
 
   
    BufferedReader br = new BufferedReader(new InputStreamReader(fis,"UTF-8"));
   // BufferedReader br = new BufferedReader(new InputStreamReader(fis));
    String strLine;
    while ((strLine = br.readLine()) != null)   
    {
        System.out.println ("\n");
        //System.out.println(strLine);
        pageRank=0;
    	//Document doc = Jsoup.connect("http://www.ebay.de/itm/MICROSOFT-XBOX-ONE-INKL-500GB-HDD-FESTPLATTE-KONSOLE-NEUWARE-BRAND-NEW-/261875788248?_trksid=p2054897.l4275").get(); 
    	Document doc = Jsoup.connect(strLine).get(); 
        String title = doc.title();  
       
        System.out.println("title is: " + title);
        
        
       Elements temp = doc.select(":matchesOwn(EUR)");
       if(temp.size()!=0)
       {
    	   System.out.println(temp.size() + " keyword EUR ");
    	   pageRank++;
    	   
       }
       
       temp = doc.select(":matchesOwn(€)");
       if(temp.size()!=0)
       {
    	   System.out.println(temp.size()+ " € Symbol");
    	   pageRank++;
    	   
       }
       
       temp = doc.select(":matchesOwn(Sternen)");
       if(temp.size()!=0)
       {
    	   System.out.println(temp.size()+ " Sternen keyword");
    	   pageRank++;
    	   
       }
       
       temp = doc.select(":matchesOwn(Produktinformationen)");
       if(temp.size()!=0)
       {
    	   System.out.println(temp.size()+ " Produktinformationen keyword");
    	   pageRank++;
    	   
       }
       
       temp = doc.select(":matchesOwn(einkaufswagen)");
       if(temp.size()!=0)
       {
    	   System.out.println(temp.size()+ " Einkaufswagen keyword");
    	   pageRank++;
    	   
       }
       
       temp = doc.select(":matchesOwn(Einkaufswagen)");
       if(temp.size()!=0)
       {
    	   System.out.println(temp.size()+ " Einkaufswagen keyword");
    	   pageRank++;
    	   
       }
       
       temp = doc.select(":matchesOwn(kaufen)");
       if(temp.size()!=0)
       {
    	   System.out.println(temp.size()+ " Kaufen keyword");
    	   pageRank++;
    	   
       }
       
       if(pageRank > 1)
       {
        	System.out.println("Its a Commerical Website Link");
       }
       else
       	System.out.println("Its not a Commerical Website Link");
       
    	
    }

  
    
  }
  
    
}
