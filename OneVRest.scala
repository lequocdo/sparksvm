package onevrest
import org.apache.spark.mllib.classification._
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD

class SVMMultiClassOVAModel(classModels: Array[SVMModel]) extends ClassificationModel with Serializable {
val classModelsWithIndex = classModels.zipWithIndex
/**
* Predict values for the given data set using the model trained.
*
* @param testData RDD representing data points to be predicted
* @return an RDD[Double] where each entry contains the corresponding prediction
*/

override def predict(testData: RDD[Vector]): RDD[Double] = {
val localClassModelsWithIndex = classModelsWithIndex
val bcClassModels = testData.context.broadcast(localClassModelsWithIndex)
testData.mapPartitions { iter =>
val w = bcClassModels.value
iter.map(v => predictPoint(v, w))
}
}

/**
* Predict values for a single data point using the model trained.
*
* @param testData array representing a single data point
* @return predicted category from the trained model
*/
override def predict(testData: Vector): Double = predictPoint(testData, classModelsWithIndex)



def predictPoint(testData: Vector, models: Array[(SVMModel, Int)]): Double = {
models
.map { case (classModel, classNumber) => (classModel.predict(testData), classNumber)}
.maxBy { case (score, classNumber) => score}
._2+1 						//since class start from 1 to 3, not 0 to 2
}

def output () = {
println("class Numbers" + classModelsWithIndex.foreach(line => println(line)))
}

}

